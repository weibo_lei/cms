package com.weibo.web;

import com.weibo.model.Dog;
import com.weibo.model.Result;
import com.weibo.service.DogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * User: leiweibo
 * Date: 13-9-5
 * Time: PM2:29
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping("/api")
public class DogController {
    private static final Logger logger = LoggerFactory.getLogger(DogController.class);
    @Autowired
    DogService service;

    @RequestMapping("/alldogs")
    @ResponseBody
    public Result getAll() {
        logger.info("The all request received.");
        return service.getAll();
    }

    @RequestMapping(method= RequestMethod.POST, value="/dog", consumes="application/json")
    public @ResponseBody String save(@RequestBody Dog dog) {
        logger.info("JSON Message:" + dog.toString());
        if(service.save(dog))
            return "upsert success";
        else
            return "upsert failed.";
    }

    @RequestMapping(method= RequestMethod.GET, value="/dog/{id}")
    public @ResponseBody String delete(@PathVariable("id") long id) {
        logger.info("delete the the dog whoese id is:" + id);
        if(service.delete(id))
            return "delete success";
        else
            return "delete failed";
    }
}
