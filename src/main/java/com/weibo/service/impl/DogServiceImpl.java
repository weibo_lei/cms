package com.weibo.service.impl;

import com.weibo.dao.DogDao;
import com.weibo.model.Dog;
import com.weibo.model.Result;
import com.weibo.service.DogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: leiweibo
 * Date: 13-9-5
 * Time: PM2:06
 * To change this template use File | Settings | File Templates.
 */

@Service
public class DogServiceImpl implements DogService {

    @Autowired
    DogDao dao;

    @Transactional
    public Result getAll() {
        return dao.getAll();
    }

    @Transactional
    public boolean save(Dog dog){
        return dao.upsert(dog);
    }

    @Transactional
    public boolean delete(long id) {
        return dao.delete(id);
    }
}
