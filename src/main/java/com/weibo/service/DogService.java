package com.weibo.service;

import com.weibo.model.Dog;
import com.weibo.model.Result;

/**
 * Created with IntelliJ IDEA.
 * User: leiweibo
 * Date: 13-9-5
 * Time: PM2:05
 * To change this template use File | Settings | File Templates.
 */
public interface DogService {
    Result getAll();
    boolean save(Dog dogInfo);
    boolean delete(long id);
}
