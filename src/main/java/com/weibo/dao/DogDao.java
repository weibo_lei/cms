package com.weibo.dao;

import com.weibo.model.Dog;
import com.weibo.model.Result;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: leiweibo
 * Date: 13-9-5
 * Time: PM2:07
 * To change this template use File | Settings | File Templates.
 */
public interface DogDao {
    Result<String, List<Dog>> getAll();
    boolean upsert(Dog dog);
    boolean delete(long id);
}
