package com.weibo.dao.impl;

import com.weibo.dao.DogDao;
import com.weibo.model.Dog;
import com.weibo.model.Result;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: leiweibo
 * Date: 13-9-5
 * Time: PM2:08
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class DogDaoImpl implements DogDao {
    private static final Logger logger = LoggerFactory.getLogger(DogDaoImpl.class);
    @Autowired
    private SessionFactory sessionFactory;

    public Result<String, List<Dog>> getAll() {
        Result<String, List<Dog>> result = new Result<String, List<Dog>>();
        List<Dog> dogs = sessionFactory.getCurrentSession().createQuery(
                "FROM Dog ORDER BY id")
                .list();
        result.setStatus("success");
        result.setObject(dogs);
        return result;
    }

    public boolean upsert(Dog dog) {
        try {
            sessionFactory.getCurrentSession().merge(dog);
            return true;
        } catch(Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    public boolean delete(long id) {
        try {
            Dog dog = new Dog();
            dog.setId(id);
            sessionFactory.getCurrentSession().delete(dog);
            return true;
        } catch(Exception e){
            logger.error(e.getMessage());
            return false;
        }
    }
}
