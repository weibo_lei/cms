package com.weibo.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: leiweibo
 * Date: 13-9-5
 * Time: PM1:59
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "dog")
@XmlRootElement(name = "dog")
public class Dog implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @NotEmpty
    private String ownerName;

    @NotEmpty
    private String phone;

    @NotEmpty
    private String address;

    @NotEmpty
    private String dogPic;

    public String getDogPic() {
        return dogPic;
    }

    public void setDogPic(String dogPic) {
        this.dogPic = dogPic;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String toString() {
        return "[id:]" + id + ", [ownername:]" + ownerName + "[address:]=" + address + "[phone:]" + phone + "[address:]" + address;
    }

}
