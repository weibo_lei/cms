package com.weibo.model;

/**
 * Created with IntelliJ IDEA.
 * User: leiweibo
 * Date: 13-8-29
 * Time: AM8:53
 * To change this template use File | Settings | File Templates.
 */
public class Result<T, V> {
    public T getStatus() {
        return status;
    }

    public void setStatus(T status) {
        this.status = status;
    }

    private T status;

    public V getObject() {
        return object;
    }

    public void setObject(V object) {
        this.object = object;
    }

    private V object;
}
